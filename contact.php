<!DOCTYPE html>
<html>
<head>
	<?php require_once('common/head.php'); ?>
</head>
<body>
	<?php require_once('common/slider.php'); ?>

	<div class="container-fliud contact_pic">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="contact_text">
						<p>GET IN TOUCH</p>
						<h2>Contact .</h2>
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="contact_num text-center">
						<i class="fa fa-map-marker" aria-hidden="true"></i>
						<h5>My Location:</h5>
						<p>chandpur,baburhat,ashikati</p>
						
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="contact_num text-center">
						<i class="fa fa-mobile" aria-hidden="true"></i>
						<h5>Phone Number:</h5>
						<p>01839072709</p>
						
					</div>
				</div>
				<div class="col-md-4">
					<div class="contact_num text-center">
						<i class="fa fa-envelope-open-o" aria-hidden="true"></i>
						<h5>Email Address:</h5>
						<p>yousufhossain50@gmail.com</p>
						
					</div>
				</div>
			</div>
		</div>

		<!-- form -->
		<form action="contact_action.php" method="post">
			<div class="contact_form">
				<div class="container">
					<div class="row">

						<div class="col-md-6 contact_name">
							<input type="text" name="name" placeholder="Name*" style="width: 100%;">
						</div>

						<div class="col-md-6 contact_mail">
							<input type="email" name="email" placeholder="Email*" style="width: 100%;">
						</div>

						<div class="col-md-12">
							<input class="sub" type="text" name="subject" placeholder="Subject" style="width: 100%;"><br>
						</div>
						
						<div class="col-md-12 message">
							<input type="text" name="msg" placeholder="Type your message" style="width: 100%;">
						</div>

						<button type="submit" class="btn btn-success" style="margin-left: 15px;">Sent message</button>
					</div>
				</div>
			</div>
		</form>
	</div>
	
	<?php require_once('common/footer.php'); ?>
</body>
</html>
