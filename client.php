<!DOCTYPE html>
<html>
<head>
	<?php require_once('common/head.php'); ?>
</head>
<body>
	<?php require_once('common/slider.php'); ?>

	<div class="container-fliud Testimonials_pic">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<div class="Testimonials">
								<p>WHAT OUR CLIENTS SAY ?</p>
								<!-- <br> -->
								<h2>Testimonials .</h2>
							</div>
							
						</div>
						<div class="col-md-12 owl-carousel">
							<div class="item Testimonials_log text-center">
								<i class="fa fa-quote-left"></i>
								<p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
								<img src="image/01.jpg" style="width: 5%; height: 50px; display: inline-block;">
								<h4>Jason Statham</h4>
								<h6>Web Desinger</h6>
							</div>

							<div class="item Testimonials_log text-center">
								<i class="fa fa-quote-left"></i>
								<p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
								<img src="image/02.jpg" style="width: 5%; height: 50px; display: inline-block;">
								<h4>Jason Statham</h4>
								<h6>Web Desinger</h6>
							</div>
							<div class="item Testimonials_log text-center">
								<i class="fa fa-quote-left"></i>
								<p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
								<img src="image/03.jpg" style="width: 5%; height: 50px; display: inline-block;">
								<h4>Jason Statham</h4>
								<h6>Web Desinger</h6>
							</div>
						</div>
					</div>
				</div>
			</div>

		<?php require_once('common/footer.php'); ?>
</body>
</html>


			
	
