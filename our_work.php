<!DOCTYPE html>
<html>
<head>
	<?php require_once('common/head.php'); ?>
</head>
<body>
	<?php require_once('common/slider.php'); ?>

	<div class="container-fliud Contact_pic">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center Contact">
						<h1>Hire me for your next project</h1>
						<button class="hire">Contact Me</button>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fliud">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="work_section">
							<p>MY RECENT WORK</p>
							<h1>My Work .</h1>
							</div>
							<div class="work_menu">
							<ul>
								<li><a href="">All</a></li>
								<li><a href="">Brand</a></li>
								<li><a href="">Disign</a></li>
								<li><a href="">Photo</a></li>
							</ul>
							
						</div>
					</div> 
				
				<!-- <div class="row"> -->
						<div class="col-md-4">
					   <div class="work_img">
							<img src="image/3.jpg" style="width: 100%;">
						</div>
						</div>
						<div class="col-md-4">
					   <div class="work_img">
							<img src="image/4.jpg" style="width: 100%;">
						</div>
						</div>
						<div class="col-md-4">
					   <div class="work_img">
							<img src="image/6.jpg" style="width: 100%;">
						</div>
						</div>
						<div class="col-md-4">
					   <div class="work_img">
							<img src="image/5.jpg" style="width: 100%;">
						</div>
						</div>
						<div class="col-md-4">
					   <div class="work_img">
							<img src="image/2.jpg" style="width: 100%;">
						</div>
						</div>
						<div class="col-md-4">
					   <div class="work_img">
							<img src="image/1.jpg" style="width: 100%;">
						</div>
						</div>
					
					</div>
				</div>
			</div>

<?php require_once('common/footer.php'); ?>

</body>
</html>
