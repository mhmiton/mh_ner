<!DOCTYPE html>
<html>
<head>
	<?php require_once('common/head.php'); ?>
</head>
<body>
	<?php require_once('common/slider.php'); ?>

	<div class="container-fliud">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center services_sectoin">
						<p>WHAT I DO?</p>
						<h1>Services .</h1>
						
					</div>
					<div class="col-md-4 service">
						<div class="icon_bar">
							<i class="fa fa-paint-brush" aria-hidden="true"></i>
						</div>
						<div class="text">
							<h4>Web design</h4>
						</div>
						<div class="text_bar">
							<p>Extreme attention to detail is the essence of Boo’s unique design concepts.</p>
						</div>

					</div>
					<div class="col-md-4 service">
						<div class="icon_bar">
							<i class="fa fa-cog" aria-hidden="true"></i>
						</div>
						<div class="text">
							<h4>Web design</h4>
						</div>
						<div class="text_bar">
							<p>Extreme attention to detail is the essence of Boo’s unique design concepts.</p>
						</div>

					</div>
					<div class="col-md-4 service">
						<div class="icon_bar">
							<i class="fa fa-camera" aria-hidden="true"></i>
						</div>
						<div class="text">
							<h4>Web design</h4>
						</div>
						<div class="text_bar">
							<p>Extreme attention to detail is the essence of Boo’s unique design concepts.</p>
						</div>

					</div>
					<div class="col-md-4 service">
						<div class="icon_bar">
							<i class="fa fa-mobile" aria-hidden="true"></i>
						</div>
						<div class="text">
							<h4>Web design</h4>
						</div>
						<div class="text_bar">
							<p>Extreme attention to detail is the essence of Boo’s unique design concepts.</p>
						</div>

					</div>
					<div class="col-md-4 service">
						<div class="icon_bar">
							<i class="fa fa-pie-chart" aria-hidden="true"></i>
						</div>
						<div class="text">
							<h4>Web design</h4>
						</div>
						<div class="text_bar">
							<p>Extreme attention to detail is the essence of Boo’s unique design concepts.</p>
						</div>

					</div>
					<div class="col-md-4 service">
						<div class="icon_bar">
							<i class="fa fa-wrench" aria-hidden="true"></i>
						</div>
						<div class="text">
							<h4>Web design</h4>
						</div>
						<div class="text_bar">
							<p>Extreme attention to detail is the essence of Boo’s unique design concepts.</p>
						</div>

					</div>
					
				</div>
				
			</div>
			
		</div>
		
		<?php require_once('common/footer.php'); ?>
</body>
</html>
