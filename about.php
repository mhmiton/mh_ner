<!DOCTYPE html>
<html>
<head>
	<?php require_once('common/head.php'); ?>
</head>
<body>
	<?php require_once('common/slider.php'); ?>

	<div class="container-fliud">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center about">
					<p>WHO I'M</p>
					<!-- <br> -->
					<h2>ABOUT ME</h2>
					
				</div>
				<div class="col-md-6 about_img">
					<img src="image/hero-about.jpg">
					
				</div>
				<div class="col-md-6 about_sit">
					<h3>Hello</h3>
					<br>
					<p>I’m <b>Jon Deo</b> . . I am a graphic designer, and I'm very passionate and dedicated to my work. With 7 years experience as a professional graphic designer, I have acquired the skills and knowledge necessary to make your project a success.</p>
					<br><br>
					<ul>
						<li>
							<span class="title">Name : </span>
							<span class="value">Md Yousuf hossain</span>
						</li>
						<li>
							<span class="title">Address : </span>
							<span class="value">baburhat</span>
						</li><br>
						<li> 
							<span class="title">Age : </span>
							<span class="value">25</span>
						</li>
						<li>
							<span class="title">Phone</span>
							<span class="value">01839072709</span>
						</li>
					</ul>
					<button class="down" a href="">Download cv</button>
					<button class="down" a href="">Hire me</button>
					
				</div>
				
			</div>
			
		</div>
	</div>
	<div class="container-fliud">
			<div class="container">
				<div class="row">
				<div class="col-md-6 make">
			<h4>Make beauty Things With Passion.</h4>
			<br>
			<p>I designer and developer services for customers of all sizes, specializing in creating stylish, modern websites, web services and online stores. My passion is to design digital user experiences through the bold interface.</p>

							<td>
					      	<button class="btn btn-success" onclick="desiner();">Read more</button>
					      	
					      </td>

					  <div id="Output"></div>
					</div>

				 <div class="col-md-6">
			 	<div class="make_form">
			 		<div class="skillbar" data-percent="98">
					  <span class="skillbar-title" style="background: #d35400;">HTML5</span>
					  <p class="skillbar-bar" style="background: #e67e22;"></p>
					  <span class="skill-bar-percent"></span>
					</div>
					<!-- End Skill Bar -->

					<div class="skillbar" data-percent="99">
					  <span class="skillbar-title" style="background: #2980b9;">CSS3</span>
					  <p class="skillbar-bar" style="background: #3498db;"></p>
					  <span class="skill-bar-percent"></span>
					</div>
					<!-- End Skill Bar -->

					<div class="skillbar" data-percent="95">
					  <span class="skillbar-title" style="background: #2c3e50;">jQuery</span>
					  <p class="skillbar-bar" style="background: #2c3e50;"></p>
					  <span class="skill-bar-percent"></span>
					</div>
					<!-- End Skill Bar -->

					<div class="skillbar" data-percent="70">
					  <span class="skillbar-title" style="background: #46465e;">PHP</span>
					  <p class="skillbar-bar" style="background: #5a68a5;"></p>
					  <span class="skill-bar-percent"></span>
					</div>
					<!-- End Skill Bar -->

			 		
			 	</div>
			 	
			 </div>
					
				</div>
				
			</div>
		</div>
		
	<?php require_once('common/footer.php'); ?>
</body>
</html>
