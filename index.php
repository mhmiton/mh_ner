<!DOCTYPE html>
<html>
<head>
	<?php require_once('common/head.php'); ?>
</head>
<body>
	<?php require_once('common/slider.php'); ?>

	<div class="container-fliud">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center about">
					<p>WHO I'M</p>
					<!-- <br> -->
					<h2>ABOUT ME</h2>
					
				</div>
				<div class="col-md-6 about_img">
					<img src="image/hero-about.jpg">
					
				</div>
				<div class="col-md-6 about_sit">
					<h3>Hello</h3>
					<br>
					<p>I’m <b>Jon Deo</b> . . I am a graphic designer, and I'm very passionate and dedicated to my work. With 7 years experience as a professional graphic designer, I have acquired the skills and knowledge necessary to make your project a success.</p>
					<br><br>
					<ul>
						<li>
							<span class="title">Name : </span>
							<span class="value">Md Yousuf hossain</span>
						</li>
						<li>
							<span class="title">Address : </span>
							<span class="value">baburhat</span>
						</li><br>
						<li> 
							<span class="title">Age : </span>
							<span class="value">25</span>
						</li>
						<li>
							<span class="title">Phone</span>
							<span class="value">01839072709</span>
						</li>
					</ul>
					<button class="down" a href="">Download cv</button>
					<button class="down" a href="">Hire me</button>
					
				</div>
				
			</div>
			
		</div>
	</div>
		
			<div class="container-fliud">
			<div class="container">
				<div class="row">
				<div class="col-md-6 make">
			<h4>Make beauty Things With Passion.</h4>
			<br>
			<p>I designer and developer services for customers of all sizes, specializing in creating stylish, modern websites, web services and online stores. My passion is to design digital user experiences through the bold interface.</p>

							<td>
					      	<button class="btn btn-success" onclick="desiner();">Read more</button>
					      	
					      </td>

					  <div id="Output"></div>

					      	
			 
		</div>
			 <div class="col-md-6">
			 	<div class="make_form">
			 		<div class="skillbar" data-percent="98">
					  <span class="skillbar-title" style="background: #d35400;">HTML5</span>
					  <p class="skillbar-bar" style="background: #e67e22;"></p>
					  <span class="skill-bar-percent"></span>
					</div>
					<!-- End Skill Bar -->

					<div class="skillbar" data-percent="99">
					  <span class="skillbar-title" style="background: #2980b9;">CSS3</span>
					  <p class="skillbar-bar" style="background: #3498db;"></p>
					  <span class="skill-bar-percent"></span>
					</div>
					<!-- End Skill Bar -->

					<div class="skillbar" data-percent="95">
					  <span class="skillbar-title" style="background: #2c3e50;">jQuery</span>
					  <p class="skillbar-bar" style="background: #2c3e50;"></p>
					  <span class="skill-bar-percent"></span>
					</div>
					<!-- End Skill Bar -->

					<div class="skillbar" data-percent="70">
					  <span class="skillbar-title" style="background: #46465e;">PHP</span>
					  <p class="skillbar-bar" style="background: #5a68a5;"></p>
					  <span class="skill-bar-percent"></span>
					</div>
					<!-- End Skill Bar -->

			 		
			 	</div>
			 	
			 </div>
					
				</div>
				
			</div>
		</div>
		<div class="container-fliud">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center services_sectoin">
						<p>WHAT I DO?</p>
						<h1>Services .</h1>
						
					</div>
					<div class="col-md-4 service">
						<div class="icon_bar">
							<i class="fa fa-paint-brush" aria-hidden="true"></i>
						</div>
						<div class="text">
							<h4>Web design</h4>
						</div>
						<div class="text_bar">
							<p>Extreme attention to detail is the essence of Boo’s unique design concepts.</p>
						</div>

					</div>
					<div class="col-md-4 service">
						<div class="icon_bar">
							<i class="fa fa-cog" aria-hidden="true"></i>
						</div>
						<div class="text">
							<h4>Web design</h4>
						</div>
						<div class="text_bar">
							<p>Extreme attention to detail is the essence of Boo’s unique design concepts.</p>
						</div>

					</div>
					<div class="col-md-4 service">
						<div class="icon_bar">
							<i class="fa fa-camera" aria-hidden="true"></i>
						</div>
						<div class="text">
							<h4>Web design</h4>
						</div>
						<div class="text_bar">
							<p>Extreme attention to detail is the essence of Boo’s unique design concepts.</p>
						</div>

					</div>
					<div class="col-md-4 service">
						<div class="icon_bar">
							<i class="fa fa-mobile" aria-hidden="true"></i>
						</div>
						<div class="text">
							<h4>Web design</h4>
						</div>
						<div class="text_bar">
							<p>Extreme attention to detail is the essence of Boo’s unique design concepts.</p>
						</div>

					</div>
					<div class="col-md-4 service">
						<div class="icon_bar">
							<i class="fa fa-pie-chart" aria-hidden="true"></i>
						</div>
						<div class="text">
							<h4>Web design</h4>
						</div>
						<div class="text_bar">
							<p>Extreme attention to detail is the essence of Boo’s unique design concepts.</p>
						</div>

					</div>
					<div class="col-md-4 service">
						<div class="icon_bar">
							<i class="fa fa-wrench" aria-hidden="true"></i>
						</div>
						<div class="text">
							<h4>Web design</h4>
						</div>
						<div class="text_bar">
							<p>Extreme attention to detail is the essence of Boo’s unique design concepts.</p>
						</div>

					</div>
					
				</div>
				
			</div>
			
		</div>
		<div class="container-fliud Contact_pic">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center Contact">
						<h1>Hire me for your next project</h1>
						<button class="hire">Contact Me</button>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fliud">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="work_section">
							<p>MY RECENT WORK</p>
							<h1>My Work .</h1>
							</div>
							<div class="work_menu">
							<ul>
								<li><a href="">All</a></li>
								<li><a href="">Brand</a></li>
								<li><a href="">Disign</a></li>
								<li><a href="">Photo</a></li>
							</ul>
							
						</div>
					</div> 
				
				<!-- <div class="row"> -->
						<div class="col-md-4">
					   <div class="work_img">
							<img src="image/3.jpg" style="width: 100%;">
						</div>
						</div>
						<div class="col-md-4">
					   <div class="work_img">
							<img src="image/4.jpg" style="width: 100%;">
						</div>
						</div>
						<div class="col-md-4">
					   <div class="work_img">
							<img src="image/6.jpg" style="width: 100%;">
						</div>
						</div>
						<div class="col-md-4">
					   <div class="work_img">
							<img src="image/5.jpg" style="width: 100%;">
						</div>
						</div>
						<div class="col-md-4">
					   <div class="work_img">
							<img src="image/2.jpg" style="width: 100%;">
						</div>
						</div>
						<div class="col-md-4">
					   <div class="work_img">
							<img src="image/1.jpg" style="width: 100%;">
						</div>
						</div>
					
					</div>
				</div>
			</div>

			<div class="container-fliud Testimonials_pic">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<div class="Testimonials">
								<p>WHAT OUR CLIENTS SAY ?</p>
								<!-- <br> -->
								<h2>Testimonials .</h2>
							</div>
							
						</div>
						<div class="col-md-12 owl-carousel">
							<div class="item Testimonials_log text-center">
								<i class="fa fa-quote-left"></i>
								<p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
								<img src="image/01.jpg" style="width: 5%; height: 50px; display: inline-block;">
								<h4>Jason Statham</h4>
								<h6>Web Desinger</h6>
							</div>

							<div class="item Testimonials_log text-center">
								<i class="fa fa-quote-left"></i>
								<p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
								<img src="image/02.jpg" style="width: 5%; height: 50px; display: inline-block;">
								<h4>Jason Statham</h4>
								<h6>Web Desinger</h6>
							</div>
							<div class="item Testimonials_log text-center">
								<i class="fa fa-quote-left"></i>
								<p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
								<img src="image/03.jpg" style="width: 5%; height: 50px; display: inline-block;">
								<h4>Jason Statham</h4>
								<h6>Web Desinger</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
		<!-- blog -->
		<div class="container-fliud my_blog">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="set_letter">
							<p>SEE LATEST NEWS</p>
							<h1>My Blog .</h1>
						</div>
					</div>
					<div class="col-md-4">
						<div class="blog_pic">
							<img src="image/1(1).jpg" style="width: 100%; height: 200px;">
							<button><a class="create" href="">Creative</a></button>
							<h5>Mobilies UX Treend</h5>
							<small>04 April 2018</small>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque</p>
						</div>
						<div class="blog">
							<img src="image/02.jpg" style="width: 50px; height: 50px;">
							<p>by jone smith</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="blog_pic">
							<img src="image/2(1).jpg" style="width: 100%; height: 200px;">
							<button><a class="create" href="">Creative</a></button>
							<h5>Mobilies UX Treend</h5>
							<small>04 April 2018</small>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque</p>
						</div>
						<div class="blog">
							<img src="image/02.jpg" style="width: 50px; height: 50px;">
							<p>by jone smith</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="blog_pic">
							<img src="image/3(1).jpg" style="width: 100%; height: 200px;">
							<button><a class="create" href="">Creative</a></button>
							<h5>Mobilies UX Treend</h5>
							<small>04 April 2018</small>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque</p>
						</div>
						<div class="blog">
							<img src="image/03.jpg" style="width: 50px; height: 50px;">
							<p>by jone smith</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- contact -->
		<div class="container-fliud contact_pic">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="contact_text">
						<p>GET IN TOUCH</p>
						<h2>Contact .</h2>
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="contact_num text-center">
						<i class="fa fa-map-marker" aria-hidden="true"></i>
						<h5>My Location:</h5>
						<p>chandpur,baburhat,ashikati</p>
						
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="contact_num text-center">
						<i class="fa fa-mobile" aria-hidden="true"></i>
						<h5>Phone Number:</h5>
						<p>01839072709</p>
						
					</div>
				</div>
				<div class="col-md-4">
					<div class="contact_num text-center">
						<i class="fa fa-envelope-open-o" aria-hidden="true"></i>
						<h5>Email Address:</h5>
						<p>yousufhossain50@gmail.com</p>
						
					</div>
				</div>
			</div>
		</div>

				<!-- form -->
				<div class="contact_form">
				<div class="container">
					<div class="row">

					<div class="col-md-6 contact_name">
						<input type="text" name="" placeholder="Name*" style="width: 100%;">
					</div>
					<div class="col-md-6 contact_mail">
						<input type="email" name="" placeholder="Email*" style="width: 100%;">
				</div>
					<div class="col-md-12">
						<input class="sub" type="text" name="" placeholder="Subject" style="width: 100%;"><br>
					</div>
						<div class="col-md-12 message">
						<input type="text" name="" placeholder="Type your message" style="width: 100%;">
					</div>
					<!-- buttom -->
					<button class="btn btn-success" style="margin-left: 15px;">Sent message</button>

					
				
			
			</div>
		</div>
		</div>
	</div>
	
	<?php require_once('common/footer.php'); ?>
</body>
</html>
