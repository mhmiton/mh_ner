<?php
	include('../common/crud.php');

	$data['skill']			= $_POST['skill'];
	$data['percentage']		= $_POST['percentage'];
	$id						= $_POST['id'];

	if(!$id)
	{
		$data['created_at']		= date('Y-m-d H:i:s');
		$save = save('skill', $data);

	    if($save == 'true')
	    {
	        echo '
				<script type="text/javascript">
					alert("Data Insert Successfully...");
					location.replace("skill.php");
				</script>
			';
	    } else {
	        echo $save;
	    }
	} else {
		$data['updated_at']		= date('Y-m-d H:i:s');
		$update = update('skill', $data, "id = '$id'");

	    if($update == 'true')
	    {
	        echo '
				<script type="text/javascript">
					alert("Data Update Successfully...");
					location.replace("skill.php");
				</script>
			';
	    } else {
	        echo $update;
	    }
	}
?>