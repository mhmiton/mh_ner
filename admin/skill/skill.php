<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include('../common/head.php'); ?>
    </head>
    <body>
        <div class="page-container">
            <?php include('../common/left_menu.php'); ?>
            
            <div class="page-content">
                <?php include('../common/header.php'); ?>

                <div class="page-inner">
                    <div class="row page-title">
                        <div class="col-md-12">
                            <div class="pull-left">
                                <h3 class="breadcrumb-header crud_title">Skill</h3>
                            </div>
                            <div class="pull-right">
                                <button type="button" class="btn btn-success crud_btn" data-toggle="modal" data-target="#myModal">Add New</button>
                            </div>
                        </div>
                    </div>
                    <div id="main-wrapper">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-white">
                                    <div class="panel-heading">
                                        <!-- <h4 class="panel-title">Skill List</h4> -->
                                    </div>
                                    <div class="panel-body">                                
                                        <div class="table-responsive">
                                            <table id="dataTable" class="table table-striped table-hover" style="border:none; width: 100%;">
                                                <thead>
                                                    <tr>
                                                        <td align="left" width="5%">SL</td>
                                                        <td align="left">Skill Name</td>
                                                        <td align="left">Percentage (%)</td>
                                                        <td align="center" width="12%">Action</td>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php
                                                        $data = read('all', 'skill', '', 'id DESC', '');
                                                        foreach($data as $key => $v) {
                                                    ?>
                                                        <tr>
                                                            <td align="left"><?php echo $key+1; ?></td>
                                                            <td align="left"><?php echo $v->skill; ?></td>
                                                            <td align="left"><?php echo $v->percentage; ?></td>
                                                            <td align="center">
                                                                <a href="#editModal<?php echo $v->id; ?>" data-toggle="modal" class="btn btn-sm btn-success"><i class="fa fa-edit"></i></a>
                                                                <a href="skill_del.php?id=<?php echo $v->id; ?>" class="btn btn-sm btn-danger"><i class="fa fa-close"></i></a>
                                                            </td>
                                                        </tr>

                                                            <div class="modal fade" id="editModal<?php echo $v->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <form action="skill_save.php" method="post"   enctype="multipart/form-data">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                                                <h4 class="modal-title" id="myModalLabel">Edit Skill</h4>
                                                                            </div>

                                                                            <div class="modal-body">
                                                                                <div class="form-group">
                                                                                    <label class="form_heading">Skill Name</label>
                                                                                    <input class="form-control" type="text" name="skill" id="" required value="<?php echo $v->skill; ?>">
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <label class="form_heading">Percentage (%)</label>
                                                                                    <input class="form-control" type="text" name="percentage" id="" required value="<?php echo $v->percentage; ?>">
                                                                                </div>

                                                                                <input type="hidden" name="id" value="<?php echo $v->id; ?>">
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                                <button type="submit" class="btn btn-success">Update</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php include('../common/footer.php'); ?>
                </div>

                <?php include('../common/right_menu.php'); ?>
            </div>
        </div>

        <!-- Modal -->
        <form id="add-row-form" action="skill_save.php" method="post" enctype="multipart/form-data">
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Add Skill</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="form_heading">Skill Name</label>
                                <input class="form-control" type="text" name="skill" id="skill" required>
                            </div>

                            <div class="form-group">
                                <label class="form_heading">Percentage (%)</label>
                                <input class="form-control" type="text" name="percentage" id="percentage" required>
                            </div>

                            <input type="hidden" name="id" value="">
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" id="add-row" class="btn btn-success">Add</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <?php include('../common/js_link.php'); ?>
    </body>
</html>