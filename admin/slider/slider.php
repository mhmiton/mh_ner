<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include('../common/head.php'); ?>
    </head>
    <body>
        <div class="page-container">
            <?php include('../common/left_menu.php'); ?>
            
            <div class="page-content">
                <?php include('../common/header.php'); ?>

                <div class="page-inner">
                    <div class="row page-title">
                        <div class="col-md-12">
                            <div class="pull-left">
                                <h3 class="breadcrumb-header crud_title">Slider</h3>
                            </div>
                            <div class="pull-right">
                                <button type="button" class="btn btn-success crud_btn" data-toggle="modal" data-target="#myModal">Add New</button>
                            </div>
                        </div>
                    </div>
                    <div id="main-wrapper">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-white">
                                    <div class="panel-heading">
                                        <!-- <h4 class="panel-title">Slider List</h4> -->
                                    </div>
                                    <div class="panel-body">                                
                                        <div class="table-responsive">
                                            <table id="dataTable" class="table table-striped table-hover" style="border:none; width: 100%;">
                                                <thead>
                                                    <tr>
                                                        <td align="left" width="5%">SL</td>
                                                        <td align="left">Slider Title</td>
                                                        <td align="left">Slider Heading</td>
                                                        <td align="left">Slider Sub-Heading</td>
                                                        <td align="center">Image</td>
                                                        <td align="center" width="12%">Action</td>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php
                                                        $data = read('all', 'slider', '', 'id DESC', '');
                                                        foreach($data as $key => $v) {
                                                    ?>
                                                        <tr>
                                                            <td align="left"><?php echo $key+1; ?></td>
                                                            <td align="left"><?php echo substr($v->title, 0,20); ?>...</td>
                                                            <td align="left"><?php echo substr($v->heading, 0,20); ?>...</td>
                                                            <td align="left"><?php echo substr($v->sub_heading, 0,20); ?>...</td>
                                                            <td align="center">
                                                                <img src="../upload/<?php echo $v->image; ?>" width="100" height="80">
                                                            </td>
                                                            <td align="center">
                                                                <a href="#editModal<?php echo $v->id; ?>" data-toggle="modal" class="btn btn-sm btn-success"><i class="fa fa-edit"></i></a>
                                                                <a href="slider_del.php?id=<?php echo $v->id; ?>" class="btn btn-sm btn-danger"><i class="fa fa-close"></i></a>
                                                            </td>
                                                        </tr>

                                                            <div class="modal fade" id="editModal<?php echo $v->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <form action="slider_save.php" method="post"   enctype="multipart/form-data">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                                                <h4 class="modal-title" id="myModalLabel">Edit Slider</h4>
                                                                            </div>

                                                                            <div class="modal-body">
                                                                                <div class="form-group">
                                                                                    <label class="form_heading">Slider Title</label>
                                                                                    <input class="form-control" type="text" name="title" id="" required value="<?php echo $v->title; ?>">
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <label class="form_heading">Slider Heading</label>
                                                                                    <input class="form-control" type="text" name="heading" id="" required value="<?php echo $v->heading; ?>">
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <label class="form_heading">Slider Sub-Heading</label>
                                                                                    <input class="form-control" type="text" name="sub_heading" id="" required value="<?php echo $v->sub_heading; ?>">
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <label class="form_heading">Slider Image</label>
                                                                                    <span class="pull-right text-success"><?php echo $v->image; ?></span>
                                                                                    <input class="form-control" type="file" name="image" id="">
                                                                                </div>

                                                                                <input type="hidden" name="id" value="<?php echo $v->id; ?>">
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                                <button type="submit" class="btn btn-success">Update</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    <?php } ?>
                                                </tbody>

                                                <!-- <tbody>
                                                    <?php
                                                        $sl = 1;
                                                        $q = mysqli_query($db, "SELECT * FROM `slider` ORDER BY id DESC");
                                                        while ($r = mysqli_fetch_assoc($q)) {
                                                    ?>
                                                        <tr>
                                                            <td align="left"><?php echo $sl++; ?></td>
                                                            <td align="left"><?php echo substr($r['title'], 0,20); ?>...</td>
                                                            <td align="left"><?php echo substr($r['heading'], 0,20); ?>...</td>
                                                            <td align="left"><?php echo substr($r['sub_heading'], 0,20); ?>...</td>
                                                            <td align="center">
                                                                <img src="../upload/<?php echo $r['image']; ?>" width="100" height="80">
                                                            </td>
                                                            <td align="center">
                                                                <a href="#editModal<?php echo $r['id']; ?>" data-toggle="modal" class="btn btn-sm btn-success"><i class="fa fa-edit"></i></a>
                                                                <a href="slider_del.php?id=<?php echo $r['id']; ?>" class="btn btn-sm btn-danger"><i class="fa fa-close"></i></a>
                                                            </td>
                                                        </tr>

                                                            <div class="modal fade" id="editModal<?php echo $r['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <form action="slider_save.php" method="post"   enctype="multipart/form-data">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                                                <h4 class="modal-title" id="myModalLabel">Edit Slider</h4>
                                                                            </div>

                                                                            <div class="modal-body">
                                                                                <div class="form-group">
                                                                                    <label class="form_heading">Slider Title</label>
                                                                                    <input class="form-control" type="text" name="title" id="" required value="<?php echo $r['title']; ?>">
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <label class="form_heading">Slider Heading</label>
                                                                                    <input class="form-control" type="text" name="heading" id="" required value="<?php echo $r['heading']; ?>">
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <label class="form_heading">Slider Sub-Heading</label>
                                                                                    <input class="form-control" type="text" name="sub_heading" id="" required value="<?php echo $r['sub_heading']; ?>">
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <label class="form_heading">Slider Image</label>
                                                                                    <span class="pull-right text-success"><?php echo $r['image']; ?></span>
                                                                                    <input class="form-control" type="file" name="image" id="">
                                                                                </div>

                                                                                <input type="hidden" name="id" value="<?php echo $r['id'] ?>">
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                                <button type="submit" class="btn btn-success">Update</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    <?php } ?>
                                                </tbody> -->
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php include('../common/footer.php'); ?>
                </div>

                <?php include('../common/right_menu.php'); ?>
            </div>
        </div>

        <!-- Modal -->
        <form id="add-row-form" action="slider_save.php" method="post" enctype="multipart/form-data">
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Add Slider</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="form_heading">Slider Title</label>
                                <input class="form-control" type="text" name="title" id="title" required>
                            </div>

                            <div class="form-group">
                                <label class="form_heading">Slider Heading</label>
                                <input class="form-control" type="text" name="heading" id="heading" required>
                            </div>

                            <div class="form-group">
                                <label class="form_heading">Slider Sub-Heading</label>
                                <input class="form-control" type="text" name="sub_heading" id="sub_heading" required>
                            </div>

                            <div class="form-group">
                                <label class="form_heading">Slider Image</label>
                                <input class="form-control" type="file" name="image" id="image" required>
                            </div>

                            <input type="hidden" name="id" value="">
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" id="add-row" class="btn btn-success">Add</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <?php include('../common/js_link.php'); ?>
    </body>
</html>