<?php
	include('../common/crud.php');

	$data['title']			= $_POST['title'];
	$data['heading']		= $_POST['heading'];
	$data['sub_heading']	= $_POST['sub_heading'];
	$id						= $_POST['id'];

	$file 					= $_FILES['image']['name'];
	if($file)
	{
		$ext 	= end(explode('.', $file));
		$name 	= time().'.'.$ext;
		move_uploaded_file($_FILES['image']['tmp_name'], '../upload/'.$name);

		$data['image']	= $name;
	}

	if(!$id)
	{
		$data['created_at']		= date('Y-m-d H:i:s');

		$save = save('slider', $data);

	    if($save == 'true')
	    {
	        echo '
				<script type="text/javascript">
					alert("Data Insert Successfully...");
					location.replace("slider.php");
				</script>
			';
	    } else {
	        echo $save;
	    }
	} else {
		// $q = mysqli_fetch_assoc(mysqli_query($db, "SELECT * FROM `slider` WHERE id = '$id'"));
		// if($q && $q['image'] && $file)
		// {
		// 	unlink('../upload/'.$q['image']);
		// }

		$q = read('one', 'slider', "id = '$id'", '', '');
		if($q && $q->image && $file)
		{
			unlink('../upload/'.$q->image);
		}

		$data['updated_at']		= date('Y-m-d H:i:s');

		$update = update('slider', $data, "id = '$id'");

	    if($update == 'true')
	    {
	        echo '
				<script type="text/javascript">
					alert("Data Update Successfully...");
					location.replace("slider.php");
				</script>
			';
	    } else {
	        echo $update;
	    }
	}
?>