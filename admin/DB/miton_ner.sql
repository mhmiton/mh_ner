-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 25, 2018 at 05:21 PM
-- Server version: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 7.2.12-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `miton_ner`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `user_name`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3'),
(2, 'shovo', 'f5a06454a67629aa4d77397f962c5592');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_category`
--

CREATE TABLE `portfolio_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `portfolio_category`
--

INSERT INTO `portfolio_category` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Web Design', '2018-11-25 17:16:01', NULL),
(2, 'Web Development', '2018-11-25 17:16:11', NULL),
(3, 'Web Application', '2018-11-25 17:16:18', NULL),
(4, 'Software', '2018-11-25 17:16:26', '2018-11-25 17:16:49');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_details`
--

CREATE TABLE `portfolio_details` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `portfolio_details`
--

INSERT INTO `portfolio_details` (`id`, `cat_id`, `title`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'Single Page Website.', '1543166250.png', 'j', '2018-11-25 17:17:06', '2018-11-25 17:17:29'),
(2, 1, 'Build a Website.', '1543166238.png', 'j', '2018-11-25 17:17:17', NULL),
(3, 3, 'Business Introduction zone', '1543166263.png', 'ikj', '2018-11-25 17:17:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `icon`, `title`, `description`, `created_at`, `updated_at`) VALUES
(3, 'fa fa-adjust', 'Hello My Name Is yousuf', 'dghfh', '2018-11-18 15:13:17', '2018-11-18 15:25:29'),
(4, 'fa fa-facebook', 'Test', 'Test', '2018-11-18 15:26:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `skill`
--

CREATE TABLE `skill` (
  `id` int(11) NOT NULL,
  `skill` varchar(255) NOT NULL,
  `percentage` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `skill`
--

INSERT INTO `skill` (`id`, `skill`, `percentage`, `created_at`, `updated_at`) VALUES
(2, 'CSS3', '80', '2018-11-15 18:32:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `heading` varchar(255) NOT NULL,
  `sub_heading` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `title`, `heading`, `sub_heading`, `image`, `created_at`, `updated_at`) VALUES
(9, 'Hello My Name Is yousuf', 'IM WEBDISINER', 'Think of a creative digital agency like dreamy in Paris, turning your dreams into reality.', '2(1).jpg', '2018-11-03 07:30:56', NULL),
(10, 'Hello My Name Is yousuf', 'I\'M WEBDISINER', 'Think of a creative digital agency like dreamy in Paris, turning your dreams into reality.', '1.jpg', '2018-11-03 07:30:37', NULL),
(11, 'Hello My Name Is yousuf', 'I\'M WEBDISINER', 'Think of a creative digital agency like dreamy in Paris, turning your dreams into reality.', '1541316163.jpg', '2018-11-03 07:19:56', '2018-11-04 07:22:43'),
(12, 'Hello My Name Is yousuf', 'I\'M WEBDISINER', 'Think of a creative digital agency like dreamy in Paris, turning your dreams into reality.', '1541316134.jpg', '2018-11-03 07:20:30', '2018-11-04 07:22:14'),
(13, 'Hello My Name Is yousuf', 'I\'M WEBDISINER', 'Think of a creative digital agency like dreamy in Paris, turning your dreams into reality.', '1541312033.jpg', '2018-11-03 07:27:41', '2018-11-04 06:13:53'),
(16, 'Test', 'Test', 'Test', '1541311850.jpg', '2018-11-03 07:36:58', '2018-11-04 06:12:52'),
(18, 'j', 'I\'M WEBDISINER', 'Think of a creative digital agency like dreamy in Paris, turning your dreams into reality.', '1541314456.jpg', '2018-11-03 07:45:50', '2018-11-04 06:54:16'),
(25, 'Hello My Name Is yousuf', 'I\'M WEBDISINER', 'Think of a creative digital agency like dreamy in Paris, turning your dreams into reality.', '1541316148.jpg', '2018-11-04 07:06:57', '2018-11-04 07:24:38'),
(26, 'Shovo', 'Chor, Pora Chor', 'Dhanda baz, kipta, kechsor, moga, foul', '1541316402.jpg', '2018-11-04 07:26:42', NULL),
(27, 'Hello My Name Is yousuf', 'I\'M WEBDISINER', 'xvxcvcxvcxv xvxcv', '1541423598.jpg', '2018-11-05 13:13:18', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio_category`
--
ALTER TABLE `portfolio_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio_details`
--
ALTER TABLE `portfolio_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skill`
--
ALTER TABLE `skill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `portfolio_category`
--
ALTER TABLE `portfolio_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `portfolio_details`
--
ALTER TABLE `portfolio_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `skill`
--
ALTER TABLE `skill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
