<?php
	include('../common/crud.php');

	$data['name']			= $_POST['name'];
	$id						= $_POST['id'];

	if(!$id)
	{
		$data['created_at']		= date('Y-m-d H:i:s');
		$save = save('portfolio_category', $data);

	    if($save == 'true')
	    {
	        echo '
				<script type="text/javascript">
					alert("Data Insert Successfully...");
					location.replace("category.php");
				</script>
			';
	    } else {
	        echo $save;
	    }
	} else {
		$data['updated_at']		= date('Y-m-d H:i:s');
		$update = update('portfolio_category', $data, "id = '$id'");

	    if($update == 'true')
	    {
	        echo '
				<script type="text/javascript">
					alert("Data Update Successfully...");
					location.replace("category.php");
				</script>
			';
	    } else {
	        echo $update;
	    }
	}
?>