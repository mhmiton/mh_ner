<?php
	include('../common/crud.php');

	$data['icon']			= $_POST['icon'];
	$data['title']			= $_POST['title'];
	$data['description']	= $_POST['description'];
	$id						= $_POST['id'];

	if(!$id)
	{
		$data['created_at']		= date('Y-m-d H:i:s');
		$save = save('service', $data);

	    if($save == 'true')
	    {
	        echo '
				<script type="text/javascript">
					alert("Data Insert Successfully...");
					location.replace("service.php");
				</script>
			';
	    } else {
	        echo $save;
	    }
	} else {
		$data['updated_at']		= date('Y-m-d H:i:s');
		$update = update('service', $data, "id = '$id'");

	    if($update == 'true')
	    {
	        echo '
				<script type="text/javascript">
					alert("Data Update Successfully...");
					location.replace("service.php");
				</script>
			';
	    } else {
	        echo $update;
	    }
	}
?>