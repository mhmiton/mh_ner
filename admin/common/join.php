<?php
    function portfolio_join()
    {
        $query = "SELECT `portfolio_details`.`*`, `portfolio_category`.`name` FROM `portfolio_details` JOIN `portfolio_category` ON `portfolio_details`.`cat_id` = `portfolio_category`.`id` ORDER BY `portfolio_details`.`id` DESC";

        $data = [];
        $q = mysqli_query($GLOBALS['db'], $query);
        while($r = mysqli_fetch_assoc($q)) {
            array_push($data, (object) $r);
        }

        if(mysqli_error($GLOBALS['db']))
        {
            return db_error($query);
        } else {
            return $data;
        }

    }
?>