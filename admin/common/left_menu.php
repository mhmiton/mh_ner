<div class="page-sidebar">
    <a class="logo-box" href="../dashboard/dashboard.php">
        <span>Space</span>
        <i class="icon-radio_button_unchecked" id="fixed-sidebar-toggle-button"></i>
        <i class="icon-close" id="sidebar-toggle-button-close"></i>
    </a>
    <div class="page-sidebar-inner">
        <div class="page-sidebar-menu">
            <ul class="accordion-menu">
                <li class="active-page">
                    <a href="../dashboard/dashboard.php">
                        <i class="menu-icon fa fa-home"></i><span>Dashboard</span>
                    </a>
                </li>

                <li class="">
                    <a href="javascript:;">
                        <i class="menu-icon fa fa-list"></i><span>Menu</span>
                    </a>
                </li>

                <li class="">
                    <a href="../slider/slider.php">
                        <i class="menu-icon fa fa-laptop"></i><span>Slider</span>
                    </a>
                </li>

                <li class="">
                    <a href="javascript:;">
                        <i class="menu-icon fa fa-info-circle"></i><span>About</span>
                    </a>
                </li>

                <li class="">
                    <a href="../skill/skill.php">
                        <i class="menu-icon fa fa-fire"></i><span>Skill</span>
                    </a>
                </li>

                <li class="">
                    <a href="../service/service.php">
                        <i class="menu-icon fa fa-handshake-o"></i><span>Service</span>
                    </a>
                </li>

                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-ravelry"></i><span>Portfolio</span>
                        <i class="accordion-icon fa fa-angle-left"></i>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="../portfolio_category/category.php">Category</a></li>
                        <li><a href="../portfolio_details/details.php">Details</a></li>
                    </ul>
                </li>

                <li class="">
                    <a href="javascript:;">
                        <i class="menu-icon fa fa-thumbs-o-up"></i><span>Testimonial</span>
                    </a>
                </li>

                <li class="">
                    <a href="javascript:;">
                        <i class="menu-icon fa fa-pencil-square"></i><span>Blog</span>
                    </a>
                </li>

                <li class="">
                    <a href="javascript:;">
                        <i class="menu-icon fa fa-share-square-o"></i><span>Social</span>
                    </a>
                </li>

                <li class="">
                    <a href="javascript:;">
                        <i class="menu-icon fa fa-key"></i><span>Change Password</span>
                    </a>
                </li>

                <li class="">
                    <a href="../logout.php">
                        <i class="menu-icon fa fa-sign-out"></i><span>Log Out</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>