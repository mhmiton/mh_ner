<?php
	include('../common/crud.php');

	$data['cat_id']			= $_POST['cat_id'];
	$data['title']			= $_POST['title'];
	$data['description'] 	= $_POST['description'];
	$id						= $_POST['id'];

	$field = ['image'];
    foreach($field as $key => $img)
    {
        if($_FILES[$img]['name'])
        {
            $path           = '../upload/';
            $name           = time()+($key+1);
            $size           = ['300', '300'];
            $convert        = 'png';
            $upload         = image($img, $name, $path, $size, $convert);
            $data[$img]     = $upload;

            if($id) { delete_img('portfolio_details', "id = '$id'", $path, $img); }
         } 
    }

	if(!$id)
	{
		$data['created_at']		= date('Y-m-d H:i:s');
		$save = save('portfolio_details', $data);

	    if($save == 'true')
	    {
	        echo '
				<script type="text/javascript">
					alert("Data Insert Successfully...");
					location.replace("details.php");
				</script>
			';
	    } else {
	        echo $save;
	    }
	} else {
		$data['updated_at']		= date('Y-m-d H:i:s');
		$update = update('portfolio_details', $data, "id = '$id'");

	    if($update == 'true')
	    {
	        echo '
				<script type="text/javascript">
					alert("Data Update Successfully...");
					location.replace("details.php");
				</script>
			';
	    } else {
	        echo $update;
	    }
	}
?>