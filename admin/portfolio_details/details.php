<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include('../common/head.php'); ?>
        <?php
            $data = portfolio_join();
        ?>
    </head>
    <body>
        <div class="page-container">
            <?php include('../common/left_menu.php'); ?>
            
            <div class="page-content">
                <?php include('../common/header.php'); ?>

                <div class="page-inner">
                    <div class="row page-title">
                        <div class="col-md-12">
                            <div class="pull-left">
                                <h3 class="breadcrumb-header crud_title">Portfolio Details</h3>
                            </div>
                            <div class="pull-right">
                                <button type="button" class="btn btn-success crud_btn" data-toggle="modal" data-target="#myModal">Add New</button>
                            </div>
                        </div>
                    </div>
                    <div id="main-wrapper">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-white">
                                    <div class="panel-heading">
                                        <!-- <h4 class="panel-title">Portfolio Details List</h4> -->
                                    </div>
                                    <div class="panel-body"> 
                                        <?php if(is_array($data)): ?>                               
                                            <div class="table-responsive">
                                                <table id="dataTable" class="table table-striped table-hover" style="border:none; width: 100%;">
                                                    <thead>
                                                        <tr>
                                                            <td align="left" width="5%">SL</td>
                                                            <td align="left">Category Name</td>
                                                            <td align="left">Title</td>
                                                            <td align="center">Image</td>
                                                            <td align="left">Description</td>
                                                            <td align="center" width="12%">Action</td>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        <?php
                                                            foreach($data as $key => $v) {
                                                        ?>
                                                            <tr>
                                                                <td align="left"><?php echo $key+1; ?></td>
                                                                <td align="left"><?php echo $v->name; ?></td>
                                                                <td align="left"><?php echo $v->title; ?></td>
                                                                <td align="center">
                                                                    <img src="../upload/<?php echo $v->image; ?>" style="width: 150px; height: 70px;">
                                                                </td>
                                                                <td align="left"><?php echo $v->description; ?></td>
                                                                <td align="center">
                                                                    <a href="#editModal<?php echo $v->id; ?>" data-toggle="modal" class="btn btn-sm btn-success"><i class="fa fa-edit"></i></a>
                                                                    <a href="details_del.php?id=<?php echo $v->id; ?>" class="btn btn-sm btn-danger"><i class="fa fa-close"></i></a>
                                                                </td>
                                                            </tr>

                                                                <div class="modal fade" id="editModal<?php echo $v->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <form action="details_save.php" method="post"   enctype="multipart/form-data">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                                                    <h4 class="modal-title" id="myModalLabel">Edit Portfolio Details</h4>
                                                                                </div>

                                                                                <div class="modal-body">
                                                                                    <div class="form-group">
                                                                                        <label class="form_heading">Select Portfolio Category</label>
                                                                                        <select class="form-control" name="cat_id" id="cat_id" required>
                                                                                            <option value="">Select Portfolio Category</option>
                                                                                            <?php
                                                                                                $cat_data = read('all', 'portfolio_category', '', '', '');
                                                                                                foreach($cat_data as $key => $cat) {
                                                                                            ?>
                                                                                                <option value="<?php echo $cat->id; ?>" <?php echo ($cat->id == $v->cat_id) ? 'selected' : '' ?>><?php echo $cat->name; ?></option>
                                                                                            <?php
                                                                                                
                                                                                                }
                                                                                            ?>
                                                                                        </select>
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label class="form_heading">Title</label>
                                                                                        <input class="form-control" type="text" name="title" id="title" required value="<?php echo $v->title; ?>">
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label class="form_heading">Image</label>
                                                                                        <span class="pull-right text-success"><?php echo $v->image; ?></span>
                                                                                        <input class="form-control" type="file" name="image" id="image">
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label class="form_heading">Description</label>
                                                                                        <textarea class="form-control" rows="5" name="description" id="description" required><?php echo $v->description; ?></textarea>
                                                                                    </div>

                                                                                    <input type="hidden" name="id" value="<?php echo $v->id; ?>">
                                                                                </div>

                                                                                <div class="modal-footer">
                                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                                    <button type="submit" class="btn btn-success">Update</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <?php else: echo $data; endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php include('../common/footer.php'); ?>
                </div>

                <?php include('../common/right_menu.php'); ?>
            </div>
        </div>

        <!-- Modal -->
        <form id="add-row-form" action="details_save.php" method="post" enctype="multipart/form-data">
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Add Portfolio Details</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="form_heading">Select Portfolio Category</label>
                                <select class="form-control" name="cat_id" id="cat_id" required>
                                    <option value="">Select Portfolio Category</option>
                                    <?php
                                        $cat_data = read('all', 'portfolio_category', '', '', '');
                                        foreach($cat_data as $key => $cat) {
                                    ?>
                                        <option value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>
                                    <?php
                                        
                                        }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="form_heading">Title</label>
                                <input class="form-control" type="text" name="title" id="title" required>
                            </div>

                            <div class="form-group">
                                <label class="form_heading">Image</label>
                                <input class="form-control" type="file" name="image" id="image" required>
                            </div>

                            <div class="form-group">
                                <label class="form_heading">Description</label>
                                <textarea class="form-control" rows="5" name="description" id="description" required></textarea>
                            </div>

                            <input type="hidden" name="id" value="">
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" id="add-row" class="btn btn-success">Add</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <?php include('../common/js_link.php'); ?>
    </body>
</html>